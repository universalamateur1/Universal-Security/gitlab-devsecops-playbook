# Contributing to Transcriptase

We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting an error
- Discussing the current state of the Playbook
- Submitting a correction
- Proposing new content

## We Develop with GitLab
We use GitLab to host this project, to track issues and feature requests, as well as accept merge requests.

## We Use [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html), So All Code Changes Happen Through [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/#merge-requests)
Merge requests are the best way to propose changes to the Playbook. We actively welcome your Merge requests:

1. Fork the repo and create your branch from `main`.
1. Make sure your contribution lints.
1. Issue that Merge request!

## Any contributions you make will be under the [Creative Commons 0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) License
In short, when you submit content changes, your submissions are understood to be under the same [Creative Commons 0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) that covers the project. Feel free to contact the maintainers if that's a concern.

## Report errors using GitLab's [issue](https://gitlab.com/universalamateur1/Universal-Security/gitlab-devsecops-playbook/-/issues)
We use GitLab issues to track public errors. Report a error by [opening a new issue](); it's that easy!

## Write bug reports with detail, background, and examples

**Great Error Reports** tend to have:

- A quick summary and/or background
- A short proof that in fact this is an error
- Notes (possibly including why you think this might be correct)
