# GitLab DevSecOps Playbook - Version 0.1a - March 2023

> Label: WIP - Work In Progress  
**[Maturity Level](https://about.gitlab.com/direction/maturity/) : `maturity: minimal`**

This playbook is designed to help you implement effective DevSecOps practices using GitLab in your company, no matter how big or small with a holistic approach.  
Our goal is to provide clear instructions and practical actionable steps for introducing security controls, measuring their effectiveness, and showing your business leaders the value of these practices.
Security mechanisms must be pervasive, simple, scalable, and easy to manage.

By following this playbook, your teams can create applications that are significantly more secure, which is ultimately what we aim for.  
Each step will have a label associated with it, indicating its level of recommendation.

1. ~"Practice::Best" Best Practice - generally accepted as superior to other known alternatives
   1. Refers to a method or technique that has been consistently proven through research, experience, and expert consensus to be the most effective and efficient way of achieving a particular goal or solving a problem. Best practices are generally widely adopted and standardized across industries and organizations as a benchmark for excellence.
2. ~"Practice::Good" Good Practice - has worked exceptionally well
   1. Refers to a method or technique that is generally accepted as effective and efficient, but may not have the same level of evidence-based support as a best practice. Good practices are typically developed through trial and error, expert opinions, and experience, and are often used as a starting point for further improvement.
3. ~"Practice::Emergent" Emergent Practice - proofed to be helpful in many cases
   1. Refers to a method or technique that is still in the experimental stage, and has not yet been fully validated or standardized. Emergent practices are often developed in response to emerging trends or new challenges, and are constantly evolving as new information becomes available.
4. ~"Practice::Novel" Novel Practice - might be a fitting solution for some issues
   1. Refers to a completely new or innovative method or technique that has not yet been widely tested or adopted. Novel practices may be developed in response to completely new challenges or opportunities, or may be the result of disruptive technologies or other paradigm shifts. While novel practices may hold great promise, they also carry a higher degree of uncertainty and risk.

## Your GitLab way

As GitLab comes in different flavors, we have compiled a [Playbook](DevSecOps-Playbook_GitLab.md) with the overview of all covered steps and actionable issue templates as checklists for every product we offer.  
Please choose the apllicable version for you to begin, when you create the issues out of the issue templates. The [different versions](https://about.gitlab.com/pricing/) have a [different feature set](https://about.gitlab.com/features/by-paid-tier/) which leads to different net playbooks. Another differentation are the [features available on Self-Managed Instances](https://about.gitlab.com/pricing/feature-comparison/).

- [itLab DevSecOps Playbook](DevSecOps-Playbook_GitLab.md)

## Inspiration

- The [DevSecOps Playbook](https://github.com/6mile/DevSecOps-Playbook) on GitHub
- The "Minimum Viable Secure Product", or [MVSP](https://mvsp.dev) is a project that tried to set a minimum baseline for what a company would need to achieve to be thought of as "secure".
- OWASP great document the [Application Security Verification Standard (ASVS)](https://github.com/OWASP/ASVS).  This document is highly technical, and should be read by anyone that is interested in AppSec.
- Timo Pagel's "DevSecOps Maturity Model" or [DSOMM](https://dsomm.timo-pagel.de/).
- GitLab's [DevSecOps Governance Framework (DGF)](https://gitlab-org.gitlab.io/professional-services-automation/pipelinecoe/pipeline-templates/#/README)
- [Security Automation SDLC](https://about.gitlab.com/handbook/security/security-engineering/automation/#implementation) by the GitLab the Security Automation Team including the [basic project scaffold template](https://gitlab.com/gitlab-com/gl-security/engineering-and-research/automation-team/scaffold-project)
- [Secure Development Lifecycle OWASP - pdf](https://owasp.org/www-pdf-archive/Jim_Manico_(Hamburg)_-_Securiing_the_SDLC.pdf)
- [OWASP Developer Guide](https://owasp.org/www-project-developer-guide/)
- [OWASP Secure Coding Practices-Quick Reference Guide](https://owasp.org/www-project-secure-coding-practices-quick-reference-guide/)
  - [Repo- OWASP Secure Coding Practices-Quick Reference Guide](https://github.com/OWASP/secure-coding-practices-quick-reference-guide)
- [Security Pattern – Source Code Management](https://securitypatterns.io/docs/01-code-mgmt-security-pattern/)
- [Secure your reposiotry](https://docs.github.com/en/code-security/getting-started/securing-your-repository)

## Direction

A Customer should be able to fork this Project into their top level AppSec Goup and create out of it issues for Playbooks which can be assigned to the implementing AppSec Engineers.

### Open ToDos

- [ ] Create Labels for [Practises](https://en.wikipedia.org/wiki/Cynefin_framework#/media/File:Cynefin_framework,_February_2011_(2).jpeg)
  - [ ] Best Practice
  - [ ] Good Practice
  - [ ] Emergent Practice
  - [ ] Novel Practice
- [ ] add free tier
- [ ] add dedicated
- [ ] Add Pipeline with MarkdownLint
- [ ] Add issue templates for task lists to implement the Playbook
  - [ ] Issue template for instance
  - [ ] Issue template for Group
  - [ ] Issue template for Project
