- [Security Policy](#security-policy)
  - [Reporting a Vulnerability](#reporting-a-vulnerability)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Security Policy

## Reporting a Vulnerability

Please report (suspected) security vulnerabilities or concerns to **[fsieverding@gitlab.com](fsieverding@gitlab.com)**. You will receive a response from us within 48 hours. If the issue is confirmed, we will release a patch as soon as possible depending on complexity but historically within a few days.
